DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero (
	sh_id serial PRIMARY KEY,
	sh_name varchar(50) NOT NULL,
	sh_alias varchar(50) NOT NULL,
	sh_origin varchar(100)
);


DROP TABLE IF EXISTS assistant; 

CREATE TABLE assistant (
	as_id serial PRIMARY KEY,
	as_name varchar(50) NOT NULL
);


DROP TABLE IF EXISTS superpower;

CREATE TABLE superpower (
	sp_id serial PRIMARY KEY,
	sp_name varchar(50) NOT NULL,
	sp_description varchar(100)
);